from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    executable = Node(
        package='my_package',
        executable='wall_follower',
        output='screen',
        remappings=[
            ('laser_scan', '/dolly/laser_scan'),
            ('cmd_vel', '/dolly/cmd_vel')
        ]
    )

    return LaunchDescription([
        executable
    ])
