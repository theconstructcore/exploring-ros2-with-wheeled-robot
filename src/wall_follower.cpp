#include "geometry_msgs/msg/twist.hpp"    // Twist
#include "rclcpp/rclcpp.hpp"              // ROS Core Libraries
#include "sensor_msgs/msg/laser_scan.hpp" // Laser Scan

using std::placeholders::_1;

class WallFollower : public rclcpp::Node {
public:
  WallFollower() : Node("WallFollower") {

    auto default_qos = rclcpp::QoS(rclcpp::SystemDefaultsQoS());
    subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "laser_scan", default_qos,
        std::bind(&WallFollower::topic_callback, this, _1));
    publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
  }

private:
  void topic_callback(const sensor_msgs::msg::LaserScan::SharedPtr _msg) {
    // 200 readings, from right to left, from -57 to 57 degress
    // calculate new velocity cmd
    float min[5] = {10, 10, 10, 10, 10};
    float current;
    int regionIndex = 0;
    for (int i = 0; i < 200; i++) {
      current = _msg->ranges[i];
      if (i < 40) {
        regionIndex = 0;
      } else if (i < 80) {
        regionIndex = 1;
      } else if (i < 120) {
        regionIndex = 2;
      } else if (i < 160) {
        regionIndex = 3;
      } else {
        regionIndex = 4;
      }
      if (current < min[regionIndex]) {
        min[regionIndex] = current;
      }
    }
    auto message = this->calculateVelMsg(min);
    publisher_->publish(message);
  }
  bool noObstacles(float distance[5]) {
    bool noObstacles = true;
    for (int i = 0; i < 5; i++) {
      if (distance[i] < 2) {
        noObstacles = false;
        break;
      }
    }
    return noObstacles;
  }
  geometry_msgs::msg::Twist calculateVelMsg(float distance[5]) {
    RCLCPP_INFO(this->get_logger(),
                "Distances are: '%.2f', '%.2f', '%.2f', '%.2f', '%.2f'",
                distance[0], distance[1], distance[2], distance[3],
                distance[4]);
    // no obstacles definition
    bool noObstacles = this->noObstacles(distance);

    // messa to be published
    auto msg = geometry_msgs::msg::Twist();
    if (distance[4] < 0.4) {
      // left distance smaller than X - turn to the right
      RCLCPP_INFO(this->get_logger(), "too close to the wall - turn right");
      //   msg.linear.x = 0.1;
      //   msg.angular.z = -0.3;
    } else if (noObstacles) {
      // no obstacles - turn to the left
      RCLCPP_INFO(this->get_logger(), "no obstacles - turn left");
      //   msg.linear.x = 0.1;
      //   msg.angular.z = 0.3;
    } else {
      // obstacles to the left - go straigh ahead
      RCLCPP_INFO(this->get_logger(), "wall to the left - go straigh ahead");
      //   msg.linear.x = 0.3;
      //   msg.angular.z = 0;
    }
    return msg;
  }
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr subscription_;
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<WallFollower>());
  rclcpp::shutdown();
  return 0;
}